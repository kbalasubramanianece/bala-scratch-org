import { LightningElement,api } from 'lwc';

export default class Ex19GrandChild extends LightningElement {
    @api grandChildName;

    @api displayHi(){
        alert('Hi There');
    }
}