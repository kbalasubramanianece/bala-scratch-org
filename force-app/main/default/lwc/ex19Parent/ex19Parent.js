import { LightningElement } from 'lwc';

export default class Ex19Parent extends LightningElement {
    
    handleClick(){
        this.template.querySelector("c-ex19-child").displayAlert();
    }

    
}