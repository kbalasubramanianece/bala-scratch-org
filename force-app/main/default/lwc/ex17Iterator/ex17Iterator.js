import { LightningElement } from 'lwc';

export default class Ex17Iterator extends LightningElement {

    contacts = [
        {
            Id: 1,
            Name: 'Bala',
            Title: 'VP of Engineering',
        },
        {
            Id: 2,
            Name: 'Jones',
            Title: 'VP of Sales',
        },
        {
            Id: 3,
            Name: 'Ilakiya',
            Title: 'CEO',
        },
    ];


}