import { LightningElement } from 'lwc';

export default class Ex22EventParent extends LightningElement {
    name;
    eventReceived(event){
        this.name=event.detail;
    }
}