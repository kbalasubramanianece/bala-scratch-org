import { LightningElement, wire } from "lwc";
import { publish, MessageContext } from "lightning/messageService";
import BALA from "@salesforce/messageChannel/Bala__c";

export default class Ex25LmsPublisher extends LightningElement {
  // page ref,channel name,message
  @wire(MessageContext)
  messageContext;
  value;

  greetingChangeHandler(event) {
    this.value = event.target.value;
  }

  handleClick() {
    const payLoad = {
      recordId: "001xx000003NGSFAA4",
      message: this.value,
      source: "LWC",
      recordData: { accountName: "Burlington Textiles Corp of America" }
    };
    publish(this.messageContext, BALA, payLoad);
  }

}