import { LightningElement } from 'lwc';
import templateOne from './templateone.html';
import templateTwo from './templatetwo.html';

export default class Ex18Render extends LightningElement {

    showTemplateOne=true;
    
    render() {
        return this.showTemplateOne ? templateOne : templateTwo;
    }

    switchTemplate() {
        this.showTemplateOne = !this.showTemplateOne;
    }
}